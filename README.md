# dotvim

Vim configuration accumulated

Backup your .vim and .vimrc settings and clone repo to ~/.vim

```bash
git clone https://colton_robertson@bitbucket.org/colton_robertson/.dot-files.git
```

**Please remember to backup your .vimrc file before running this script**
```bash
$ cd ~/.vim
```
